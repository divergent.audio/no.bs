#!/bin/bash
clang++ -std=c++2a -Ofast -DRANGES_MODULES -DXXH_INLINE_ALL -DBOOST_ASIO_HAS_STD_STRING_VIEW -Wno-logical-op-parentheses -I$HOME/zenAud.io/lib/dist/include -L"$HOME/zenAud.io/lib/dist/lib" -Xlinker "-rpath" -Xlinker "$HOME/zenAud.io/lib/dist/lib" -lboost_thread -lboost_filesystem -lboost_system no.bs.cpp -o no.bs
